import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import store from "./store";

Vue.config.productionTip = false;

Vue.filter("formatDate", function(value) {
  if (value) {
    return (
      new Date(parseInt(value)).toString().substring(3, 10) +
      new Date(parseInt(value)).toString().substring(15, 21)
    );
  }
});

new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
