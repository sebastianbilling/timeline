import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import VueAxios from "vue-axios";

Vue.use(Vuex);
Vue.use(VueAxios, axios);

export default new Vuex.Store({
  state: {
    items: [],
    newAuthor: "",
    newMessage: ""
  },

  actions: {
    loadItems({ commit }) {
      axios
        .get("http://linusnyren.ddns.net:8080/GuestBook/GuestBook/GuestBook")
        .then(r => r.data)
        .then(items => {
          commit("SET_ITEMS", items);
        });
    },

    addItem({ commit, state }) {
      let config = {
        headers: { "Content-Type": "application/json" }
      };

      axios
        .post(
          "http://linusnyren.ddns.net:8080/GuestBook/GuestBook/GuestBook",
          {
            author: state.newAuthor,
            message: state.newMessage
          },
          config
        )
        .then(() => {
          commit({
            type: "ADD_ITEM",
            author: state.newAuthor,
            message: state.newMessage
          });
        });
    },

    removeItem({ commit }, id) {
      axios
        .delete(
          "http://linusnyren.ddns.net:8080/GuestBook/GuestBook/GuestBook/",
          {
            data: {
              id: id
            },
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .then(() => {
          commit({
            type: "REMOVE_ITEM",
            id: id
          });
        });
    }
  },
  mutations: {
    SET_ITEMS(state, items) {
      state.items = items;
    },

    ADD_ITEM(state, noteObject) {
      state.items.push(noteObject);
    },
    REMOVE_ITEM(state, noteObject) {
      let index = -1;
      for (let i = 0; i < state.items.length; i++) {
        if (state.items[i].id === noteObject.id) {
          index = i;
          break;
        }
      }
      state.items.splice(index, 1);
    },

    UPDATE_MESSAGE(state, newMessage) {
      state.newMessage = newMessage;
    },

    UPDATE_AUTHOR(state, newAuthor) {
      state.newAuthor = newAuthor;
    }
  }
});
