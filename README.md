Part of the Serverprogrammering course, frontend made with VueJS and the backend made with Java.

Website can be found at:
http://linusnyren.ddns.net/Website/index.html


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
